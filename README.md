# Spotify Jmeter Project consists of 2 parts.

###Every part has prerequisites:
* Thread group with 30 threads;
* HTTP Header Manager with Authorization token;
* Constant Timer with random Thread delay up to 10 seconds;

### 1st part - is GET search request on Spotify service using endpoint:
```
https://api.spotify.com/v1/search
```

### 2nd part - is POST create playlist request on Spotify service using endpoint:
```
https://api.spotify.com/v1/users/{user_id}/playlists
```

###Every HTTP request sampler consists of Listeners:
* View results Tree, where could be found response for every request with body;
* Summary report with statistic for all Samples(requests) with min, max and average results in ms for 1 sample. and % of errors;
* Graph Results with: number of requests, the average response time of total requests, the deviation from average time, throughput of requests per minute;
* Active threads over time;

###Every HTTP request sampler consists of Assertions:
* Response Assertion Code;
* Response Assertion Message;
* Response Assertion Text Response / JSON Assertion (that checks that response body consists of expected values).

###To create html report for Spotify Jmeter Test Plan is used command:
```
jmeter -n -t(path of .jmx file) -l(path of examples folder along with name of csv file where you want to keep the results) -e -o(path of output folder where you want to save the results)
```
###The example could be find here:
https://haquemousume.medium.com/learn-to-generate-and-analyse-html-intuitive-reports-in-jmeter-574dbe1fca72
